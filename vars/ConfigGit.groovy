#!/usr/bin/env groovy

import modules.Git

def call(String mail){
    return new Git(this).Config(mail)
}
