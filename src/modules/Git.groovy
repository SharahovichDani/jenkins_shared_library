#!/usr/bin/env groovy

package modules

class Git implements Serializable {
    def pack

    Git(pack) {
        this.pack = pack
    }
    def PushGitLab(String url) {
        pack.withCredentials([pack.usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            pack.env.encodedPass = URLEncoder.encode(pack.PASS, "UTF-8")
            pack.sh '''git remote set-url origin https://'''+pack.USER+''':'''+pack.encodedPass+'''@'''+url+''''''
            pack.sh 'git add .'
            pack.sh 'git commit -m "[ci skip]"'
            pack.sh 'git push origin HEAD:main'
        }
    }
    def PushGitHub(String url) {
        pack.withCredentials([pack.string(credentialsId: "github-credentials", variable: 'PASS')]) {
            pack.sh '''git remote set-url origin https://'''+pack.PASS+'''@'''+url+''''''
            pack.sh 'git add .'
            pack.sh 'git commit -m "[ci skip]"'
            pack.sh 'git push origin HEAD:main'
        }
    }
    def Config(String mail) {
        pack.sh '''git config --global user.email '''+mail+''''''
        pack.sh 'git config --list'
    }

}
