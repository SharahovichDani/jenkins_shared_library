# Shared Library Automation Build NPM Application

## Introduction

This is an automated declarative Jenkins pipeline that builds and increments versions with Shared Library. The project is based on Jenkins, Shared Library, NPM, and Docker.

As a result of building the pipeline, the application will be connected to the shared library and updated, the app will be built, the app will be pushed to the Docker hub, and the repository will be updated in Git.









